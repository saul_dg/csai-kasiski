from functools import reduce
from math import gcd, sqrt
from sys import argv
from collections import defaultdict, Counter

LANGUAGES_REL_FREQ = {
    'English': {'A': 0.08167, 'B': 0.01492, 'C': 0.02782, 'D': 0.04253, 'E': 0.12702, 'F': 0.02228, 'G': 0.02015,
                   'H': 0.06094, 'I': 0.06966, 'J': 0.00153, 'K': 0.00772, 'L': 0.04025, 'M': 0.02406, 'N': 0.06749,
                   'O': 0.07507, 'P': 0.01929, 'Q': 0.00095, 'R': 0.05987, 'S': 0.06327, 'T': 0.09056, 'U': 0.02758,
                   'V': 0.00978, 'W': 0.02360, 'X': 0.00150, 'Y': 0.01974, 'Z': 0.00074},
    "Spanish": {'A': 0.1172, 'B': 0.0149, 'C': 0.0387, 'D': 0.0467, 'E': 0.1372, 'F': 0.0069, 'G': 0.01,
                   'H': 0.0118, 'I': 0.0528, 'J': 0.0052, 'K': 0.0011, 'L': 0.0524, 'M': 0.0308, 'N': 0.0683, 'Ñ': 0.0017,
                   'O': 0.0844, 'P': 0.0289, 'Q': 0.0111, 'R': 0.0641, 'S': 0.0720, 'T': 0.0460, 'U': 0.0455,
                   'V': 0.0105, 'W': 0.0004, 'X': 0.0014, 'Y': 0.0109, 'Z': 0.0047},
    "French": {'A': 0.0813, 'B': 0.0093, 'C': 0.0315, 'Ç': 0.0001, 'D': 0.0355, 'E': 0.1510, 'F': 0.0024, 'G': 0.0097,
                   'H': 0.0108, 'I': 0.0694, 'J': 0.0071, 'K': 0.0016, 'L': 0.0568, 'M': 0.0323, 'N': 0.0642,
                   'O': 0.0527, 'P': 0.0303, 'Q': 0.0089, 'R': 0.0643, 'S': 0.0791, 'T': 0.0711, 'U': 0.0605,
                   'V': 0.0183, 'W': 0.0004, 'X': 0.0042, 'Y': 0.0019, 'Z': 0.0021}
}


def alphabet_diff(a, b, dictionary, char_to_alphabet_index):
    """Compute (a - b) inside alphabet."""
    return dictionary[(char_to_alphabet_index[a] - char_to_alphabet_index[b]) % len(dictionary)]


def decrypt(encryption, key, dictionary, char_to_alphabet_index):
    return ''.join(alphabet_diff(encryption[i], key[i % len(key)], dictionary, char_to_alphabet_index) for i in range(len(encryption)))


def kasiski_examination(ciphertext, seq_len=5):
    # Find positions of each substring of length `seq_len`
    seq_positions = defaultdict(list)  # {seq: [pos]}
    for i in range(len(ciphertext) - seq_len):
        seq_positions[ciphertext[i: i + seq_len]].append(i)

    seq_positions = {
        seq: positions
        for seq, positions in seq_positions.items()
        if len(positions) > 1
    }

    # Calculate spacings between subsequent positions for each sequence
    seq_spacings = set()  # {seq: [space]}
    for seq, positions in seq_positions.items():
        for a, b in zip(positions, positions[1:]):
            seq_spacings.add(b - a)

    key_len = reduce(gcd, seq_spacings)

    return key_len


def to_blocks(ciphertext, key_len):
    return [''.join(ciphertext[shift + i]
                    for i in range(0, len(ciphertext) - shift, key_len))
            for shift in range(key_len)]


def combine_dicts(dict1, dict2):
    combined_dict = {}
    all_keys = set(dict1.keys()) | set(dict2.keys())  # Union of keys
    for key in all_keys:
        value1 = dict1.get(key, 0)  # Use 0 as default if key not in dict1
        value2 = dict2.get(key, 0)  # Use 0 as default if key not in dict2
        combined_dict[key] = value1 + value2
    return combined_dict


def cosine_similarity(vec1, vec2):
    """
    Compute the cosine similarity between two vectors.
    """
    dot_product = sum(vec1[key] * vec2[key] for key in vec1)
    magnitude1 = sqrt(sum(vec1[key] ** 2 for key in vec1))
    magnitude2 = sqrt(sum(vec2[key] ** 2 for key in vec2))
    if magnitude1 == 0 or magnitude2 == 0:
        return 0
    return dot_product / (magnitude1 * magnitude2)


def frequency_analysis(ciphertext, key_len, lang_freq, dictionary):
    final_key = ''
    lang_final_similarity = 0
    char_to_alphabet_index = dict(zip(dictionary, range(len(dictionary))))
    for lang in lang_freq.values():
        lang = Counter(lang)
        blocks = to_blocks(ciphertext, key_len)
        block_freqs = [Counter(block) for block in blocks]
        most_common_letters = lang.most_common(3)

        expected_freqs = {char: freq for char, freq in lang.items()}

        key = ''
        observed_freqs_f = {}

        for i, block in enumerate(blocks):
            final_similarity = 0
            observed_freqs_t = {}
            letter = ''
            for common_letter in most_common_letters:
                ci = block_freqs[i].most_common()[0][0]
                ki = alphabet_diff(ci, common_letter[0][0], dictionary, char_to_alphabet_index)

                m = decrypt(block, ki, dictionary, char_to_alphabet_index)

                observed_freqs = Counter(m)
                similarity_error = cosine_similarity(expected_freqs, observed_freqs)
                if similarity_error > final_similarity:
                    letter = ki
                    observed_freqs_t = observed_freqs
                    final_similarity = similarity_error

            key += letter
            observed_freqs_f = combine_dicts(observed_freqs_f, observed_freqs_t)
        if key != '':
            observed_freqs = Counter(observed_freqs_f)
            lang_similarity = cosine_similarity(expected_freqs, observed_freqs)
            if lang_similarity > lang_final_similarity:
                final_key = key
                lang_final_similarity = lang_similarity
    return final_key


if __name__ == "__main__":
    with open(argv[1], "r", encoding="utf-8") as file:
        lines = file.read().upper()
    message, dictionary = lines.split("\n")
    key_len = kasiski_examination(message, seq_len=7)
    key = frequency_analysis(message, key_len, LANGUAGES_REL_FREQ, dictionary)
    print(key)
